<?php

namespace App\Http\Middleware;

use Auth;
use App\Role;
use Closure;


class Staff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->authorizeRoles('staff')) {
            return redirect('/staff');
        }

        return $next($request);
        
    }
}
