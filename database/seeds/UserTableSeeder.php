<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff_role = Role::where('name', 'staff')->first();
        $manager_role  = Role::where('name', 'manager')->first();
        $staff = new User();
        $staff->name = 'Staff Name';
        $staff->email = 'staff@example.com';
        $staff->password = bcrypt('secret');
        $staff->save();

        $staff->roles()->attach($staff_role);
        $manager = new User();
        $manager->name = 'Manager Name';
        $manager->email = 'manager@example.com';
        $manager->password = bcrypt('secret');
        $manager->save();
        $manager->roles()->attach($manager_role);
    }
}
