<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
      $staff_role = new Role();
      $staff_role->name = 'staff';
      $staff_role->description = 'A Staff';
      $staff_role->save();
      $manager_role = new Role();
      $manager_role->name = 'manager';
      $manager_role->description = 'A Manager-SuperAdmin';
      $manager_role->save();
    }

}
